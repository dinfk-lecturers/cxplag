import logging
import pkgutil
import os
from .utils import logging as logutils

def main():
  logutils.configure_level_and_format()

  # Files hardcoded here because I couldn't figure out how to iterate over the
  # files in the package's "data" directory.
  # See also https://stackoverflow.com/questions/61531935.
  files = [
    "diff.html.jinja",
    "pyg-algol_nu.css",
    "clusters.html.jinja",
    "cluster_mail.txt"
  ]

  source_data_directory = "./data"
  destination_data_directory = "./_static"

  logging.info("Creating directory {}".format(destination_data_directory))
  os.makedirs(destination_data_directory, exist_ok=True)

  for file in files:
    source_file = os.path.join(source_data_directory, file)
    destination_file = os.path.join(destination_data_directory, file)

    logging.info("Creating file {}".format(destination_file))

    with open(destination_file, "wb") as destination_fh:
      destination_fh.write(pkgutil.get_data(__name__, source_file))


if __name__ == "__main__":
  main()
