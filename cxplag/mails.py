import logging
import mailbox
import email
import argparse
from email.message import EmailMessage
from email.headerregistry import Address
# import quopri
import os
import pathlib
import glob
import csv
import datetime
import re
from .utils import logging as logutils

DEFAULT_CLUSTER_FILES_DIR="_clusters"
DEFAULT_CLUSTER_STUDENTS_CSV_FILE_GLOB="cluster-students-[0-9]*.csv"
DEFAULT_MAIL_BODY_FILE="./_static/cluster_mail.txt"
DEFAULT_STUDENTS_RECEIVE_MAILS_AS_HEADER="To" # To, CC, BCC

def create_emails(
    cluster_files_dir,
    cluster_students_csv_file_glob,
    mail_body_file,
    students_receive_mails_as_header,
    sender_address,
    subject,
    mbox_filename):

  logutils.configure_level_and_format()

  mbox_path = pathlib.Path(mbox_filename)
  mbox_msf_path = pathlib.Path(mbox_filename + ".msf")

  logging.warn("ATTENTION: If you continue, the files")
  logging.warn("  {}".format(mbox_path))
  logging.warn("  {}".format(mbox_msf_path))
  logging.warn("will be overwritten, if they exist. Make sure that do not have any pending unsent messages!")
  
  if (input("Do you want to continue (enter 'yes')? ").lower() != "yes"):
    logging.debug("Exiting program because user did not confirm to continue")
    exit(0)

  # Delete mailbox file and corresponding summary/index file
  mbox_path.unlink(missing_ok=True)
  mbox_msf_path.unlink(missing_ok=True)

  logging.info("Reading mail body from file {}".format(mail_body_file))
  logging.info("IMPORTANT: File encoding must be UTF-8!")
  with open(mail_body_file, encoding="utf-8") as body_fh:
    mail_body = body_fh.read()

  mbox = mailbox.mbox(mbox_path)
  mbox.lock()
  try:
    cluster_students_glob = os.path.join(cluster_files_dir, cluster_students_csv_file_glob)
    logging.info("Taking students per cluster from files {}".format(cluster_students_glob))

    cluster_csv_files = glob.glob(cluster_students_glob)
    logging.info("Found {} matching cluster files".format(len(cluster_csv_files)))

    for students_per_cluster_csv in cluster_csv_files:
      with open(students_per_cluster_csv, newline='') as csv_fh:
        cluster_csv = list(csv.DictReader(csv_fh))

      recipients = [get_recipient(row) for row in cluster_csv]

      # test_addresses = ["foo@bar.com"]
      # recipients = [Address(addr_spec=addr) for adrr in test_addresses]
      
      logging.debug("Creating mail to {}".format(", ".join([str(r) for r in recipients])))

      msg = EmailMessage()
      msg["From"] = sender_address
      msg["To"] = recipients
      msg["Subject"] = subject
      msg['Date'] = email.utils.localtime()
      msg.set_content(mail_body)

      mbox.add(msg)
      mbox.flush()

  finally:
    mbox.unlock()


def configure_cli_parser(parser):
  parser.add_argument(
    "-f", "--from",
    dest="sender", # Create attribute 'sender', since 'from' is a keyword
    type=str,
    help="E-mail sender name and address, as 'Name <user@server.tld>'",
    required=True)

  parser.add_argument(
    "-s", "--subject",
    type=str,
    help="E-mail subject",
    required=True)

  parser.add_argument(
    "-m", "--mbox-file",
    type=str,
    help="Target MBox file for e-mails to create. E.g. 'C:\\Program Files\\Thunderbird\\Data\\profile\\Mail\\Local Folders\\Unsent Messages'.",
    required=True)

  logutils.add_loglevel_argument(parser)

def get_recipient(cluster_csv_row) -> Address:
  email = cluster_csv_row["Email"].strip()

  if (not email):
    legi = cluster_csv_row["Legi"]
    email = "MISSING@UNKNOWN.CH"
    logging.warn("E-mail address for {} is missing, using {} instead".format(legi, email))

  return Address(addr_spec=email)


def main(
    cluster_files_dir=DEFAULT_CLUSTER_FILES_DIR,
    cluster_students_csv_file_glob=DEFAULT_CLUSTER_STUDENTS_CSV_FILE_GLOB,
    mail_body_file=DEFAULT_MAIL_BODY_FILE,
    students_receive_mails_as_header=DEFAULT_STUDENTS_RECEIVE_MAILS_AS_HEADER):

  parser = argparse.ArgumentParser()
  configure_cli_parser(parser)
  args = parser.parse_args()

  logutils.configure_level_and_format(args.log_level)


  sender_pattern = r"\s*(.+?)\s*<(.+?)@(.+?)>"
  sender_match = re.search(sender_pattern, args.sender)

  assert \
    sender_match and sender_match.lastindex == 3, \
    "Argument provided for --format does not match expected pattern"

  sender_address = Address(*sender_match.groups())

  create_emails(
    cluster_files_dir,
    cluster_students_csv_file_glob,
    mail_body_file,
    students_receive_mails_as_header,
    sender_address,
    args.subject,
    args.mbox_file)


if __name__ == "__main__":
  main()
