import os
import argparse
import logging
import csv
import subprocess
import json
import dataclasses
import networkx as nx
from .utils import logging as logutils

## TODO: cluster.py could create a first, less detailed version of the
##       clusters.html report, by extracting the strictly necessary information
##       (student name and e-mail address) from the details.json file located
##       in the CX export. This information would already be enough to generate
##       e-mails afterwards.
##       aggr.py would then be optional, if a more detailed cluster report is desired.
##
## TODO: Generate DOT, SVG and CSV files in a subdirectory, e.g. "_clusters"

DEFAULT_COMPARISON_FILE_FORMAT="_diffs/{}-vs-{}.html"
DEFAULT_JPLAG_RESULTS_OVERVIEW_FILE= "_jplag_results/overview.json"
DEFAULT_TOTAL_GRAPH_DOT_FILE="jplag-report.dot"
DEFAULT_CLUSTER_FILES_DIR="_clusters"
DEFAULT_CLUSTER_FILE_PATTERN="cluster-{}.{}"
DEFAULT_THRESHOLD_PERCENTAGE=90
DEFAULT_CREATE_SVG_FILES=True
DEFAULT_CLUSTERS_MATCHES_CSV_FILE="clusters-matches.csv"

@dataclasses.dataclass
class JPlagResult:
  id1: str
  id2: str
  similarity: int
  match_file: str

  def fields_flattened(self):
    return [f.name for f in dataclasses.fields(self)]

  def values_flattened(self):
    return dataclasses.astuple(self)

@dataclasses.dataclass
class ClusterEntry:
  cluster_id: int
  result: JPlagResult
  dot_file: str
  svg_file: str

  def fields_flattened(self):
    field_names = [f.name for f in dataclasses.fields(self)]
    result_headers = self.result.fields_flattened()

    return field_names[:1] + result_headers + field_names[2:]

  def values_flattened(self):
    field_data = dataclasses.astuple(self)
    result_data = self.result.values_flattened()

    return field_data[:1] + result_data + field_data[2:]


def read_results_from_jplag_overview_file(
    overview_json_file,
    comparison_file_format):
  
  results = []

  logging.info("Reading results from {}".format(overview_json_file))

  with open(overview_json_file, "r") as f:
    data = json.load(f)
    edges = data["metrics"][0]["topComparisons"]

  for edge in edges:
    id1 = str(edge["first_submission"])
    id2 = str(edge["second_submission"])
    sim = int(edge["similarity"] * 100)
    match_file = comparison_file_format.format(id1, id2)
    results.append(JPlagResult(id1, id2, sim, match_file))

  logging.debug("Read {} results".format(len(results)))

  return results


def get_weight(result):
  return result.similarity


def get_color(similarity):
  if similarity >= 90:
    return "#D83018"  # Red
  elif similarity >= 80:
    return "#F07241"  # Orange
  elif similarity >= 70:
    return "#601848"  # Purple
  else:
    return "#000000"  # Black


def include(result, similarity_threshold):
  return similarity_threshold <= get_weight(result)


def get_results_graph(results, percentage_threshold):
  graph = nx.Graph()

  logging.debug("Creating graph from {} initial results".format(len(results)))
  logging.debug("Threshold similarity: ".format(percentage_threshold))

  for result in results:
    if not include(result, percentage_threshold):
      continue

    weight = get_weight(result)
    edge = (result.id1, result.id2, weight)
    color = get_color(weight)

    # TODO: Don't hardcode ../ path prefix
    match_url = "../{}".format(result.match_file)

    attributes = {
      # Attributes for GraphViz
      "color": color,
      "penwidth": 2,
      "label": "{0}%".format(weight),
      "labelURL": match_url,
      "URL": match_url,
      "target": "match",
      "fontcolor": color,
      # Attributes for internal bookkeeping
      "_result": result
    }

    graph.add_weighted_edges_from([edge], **attributes)

  logging.debug(
    "Graph contains {} nodes and {} edged".format(
      graph.number_of_nodes(),
      graph.number_of_edges()))

  return graph


def create_cluster_dot_and_svg_files(subgraph, index, cluster_dot_file, cluster_svg_file=None):
  logging.debug(
    "Writing cluster {} with {}/{} nodes/edge to file {}".format(
      index,
      subgraph.number_of_nodes(),
      subgraph.number_of_edges(),
      cluster_dot_file))

  nx.drawing.nx_pydot.write_dot(subgraph, cluster_dot_file)

  if cluster_svg_file:
    dot_command = ["dot", "-Tsvg", "-o{}".format(cluster_svg_file), cluster_dot_file]

    logging.debug("Calling dot to create SVG {} file from {}".format(cluster_svg_file, cluster_dot_file))
    logging.debug("Command: {}".format(" ".join(dot_command)))

    subprocess.run(dot_command)


def create_clusters(graph, cluster_file_pattern, create_svg_files):
  logging.info("Computing connected component (CC) clusters")
  clusters = sorted(nx.connected_components(graph), key=len, reverse=True)

  logging.info(
    "Found {} CC clusters, will write them to files {}".format(
      len(clusters),
      cluster_file_pattern.format("#", "dot")))

  cluster_entries = []

  for index, cluster in enumerate(clusters):
    subgraph = graph.subgraph(cluster).copy()
    dot_file = cluster_file_pattern.format(index, "dot")

    svg_file = None
    if create_svg_files:
      svg_file = cluster_file_pattern.format(index, "svg")

    create_cluster_dot_and_svg_files(subgraph, index, dot_file, svg_file)

    for (_, _, data) in subgraph.edges(data=True):
      cluster_entries.append(
        ClusterEntry(
          index,
          data["_result"],
          dot_file,
          svg_file))

  return cluster_entries


def create_clusters_matches_csv_file(cluster_entries, clusters_matches_csv_file):
  logging.info("Writing file with matches per clusters {}".format(clusters_matches_csv_file))

  if cluster_entries:
    with open(clusters_matches_csv_file, "w", newline="") as csv_fh:
      csv_writer = csv.writer(csv_fh)

      csv_writer.writerow(cluster_entries[0].fields_flattened())

      for entry in cluster_entries:
        csv_writer.writerow(entry.values_flattened())


def configure_cli_parser(parser):
  parser.add_argument(
    "-tp", "--threshold-percentage",
    type=int,
    default=DEFAULT_THRESHOLD_PERCENTAGE,
    help="Threshold for similarity in percentage; matches below will be excluded (default: {})".format(DEFAULT_THRESHOLD_PERCENTAGE))

  logutils.add_loglevel_argument(parser)

def main(
    comparison_file_format=DEFAULT_COMPARISON_FILE_FORMAT,
    jplag_results_overview_file=DEFAULT_JPLAG_RESULTS_OVERVIEW_FILE,
    total_graph_dot_file=DEFAULT_TOTAL_GRAPH_DOT_FILE,
    cluster_files_dir=DEFAULT_CLUSTER_FILES_DIR,
    cluster_file_pattern=DEFAULT_CLUSTER_FILE_PATTERN,
    create_svg_files=DEFAULT_CREATE_SVG_FILES,
    clusters_matches_csv_file=DEFAULT_CLUSTERS_MATCHES_CSV_FILE):

  parser = argparse.ArgumentParser()
  configure_cli_parser(parser)
  args = parser.parse_args()

  logutils.configure_level_and_format(args.log_level)

  percentage_threshold=args.threshold_percentage

  results = read_results_from_jplag_overview_file(jplag_results_overview_file, comparison_file_format)
  graph = get_results_graph(results, percentage_threshold)

  logging.info("Writing total graph to {}".format(total_graph_dot_file))
  nx.drawing.nx_pydot.write_dot(graph, total_graph_dot_file)

  logging.info("Creating directory {}".format(cluster_files_dir))
  os.makedirs(cluster_files_dir, exist_ok=True)

  cluster_entries = create_clusters(
      graph,
      os.path.join(cluster_files_dir, cluster_file_pattern),
      create_svg_files)

  create_clusters_matches_csv_file(cluster_entries, clusters_matches_csv_file)


if __name__ == "__main__":
  main()
