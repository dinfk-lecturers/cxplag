# cxplag

Scripts for checking Code Expert submissions for code plagiarism with JPlag.

## Installation

### cxplag

```shell
$ pip install --upgrade git+https://gitlab.ethz.ch/dinfk-lecturers/cxplag.git
...
Successfully installed [...] cxplag [...]
```

Argument `--upgrade` (re)installs the package, even if the version number hasn't changed.

During development, i.e. when changing a local checkout of `cxplag`'s sources,  install via `pip install --upgrade -e /path/to/checkout` to always run the latest version.

### JPlag

Download `jplag-4.2.0-jar-with-dependencies.jar` from https://github.com/jplag/jplag/releases.

**Note:** JPlag 4.3.0 works as well. It provides a new C++ module (`cpp2`, see further down) that seems to produce *shockingly different results*.

## Usage

### Step 1: Preprocessing

**Note:** Create a directory per exercise to check, and keep the submissions in a subdirectory thereof. E.g. `~/ex1/submissions` (see also step 2 further down).

1. Obtain and prepare data, see `preprocessing/README.md`.

1. If you want to include additional submissions, e.g. a master solution, create a separate user directory per additional submission, and place the submission code there. E.g. create `~/ex1/submissions/00-000-001/main.cpp`, where user `00-000-001` does not exist otherwise. Corresponding `details.json` files are optional.

1. Open a shell one level above the directory that contains the user submissions, i.e. the `<user>` directories from `preprocessing/README.md`. E.g. if you have `~/ex1/submissions/<user>`, then `cd ~/ex1`.

### Step 2: JPlag

**Warning:** It seems that JPlag fails to parse files with non-standard characters! E.g. a C++ file with variable `überprüfen`, and another with string literal `"—"` (not the regular minus character). JPlag logs corresponding warnings, but they drown in all the output. It is *strongly recommended* to pipe JPlag's output into a text file and then grep for `[WARN]`. In the observed case, the parsing failures nearly prevented detecting a plagiarism case with very high similarity.

1. Create directory `~/ex1/template` and add the template file that was initially given to all students. Note that this is required by JPlag (because all code in this initial file is shared by all students)!

1. Linux users (at least Ubuntu 20.04), ensure that the `.jar` file `jplag-4.2.0-jar-with-dependencies.jar` is executable, by running: 

   ```shell
   $ chmod u+x jplag-4.2.0-jar-with-dependencies.jar`
   ```

1. If the code exercise is in C++ (extension `.cpp`), run

   ```shell
   $ java -jar jplag-4.2.0-jar-with-dependencies.jar ./submissions/ -l cpp -bc ./template/
   ```

   **Note:** JPlag 4.3.0's new C++ module `-l cpp2` seems to be *much* more sensitive to changes, i.e. report *many* fewer high-similarity pairs! I (Malte) have tested it on one bonus exercise (where the solution space was pretty small, to be fair) and `cpp` reported 126 plagiarists with a threshold of 90% (95/100%), whereas `cpp2` reported 23/50%. One difference seems to be that `cpp` treats `ptr++`, `std::next(ptr)` and `std::next(ptr, 1)` as equivalent, and also `std::swap(*p, *q)` and `std::iter_swap(p, q)`.

   If the code exercise is in Python (extension `.py`), run

   ```shell
   $ java -jar jplag-4.2.0-jar-with-dependencies.jar ./submissions/ -l python3 -bc ./template/
   ```

   **Note:** By default, JPlag's report includes only 100 comparisons. Pass `-n <N>` for `<N>` comparisons; `-1` for all.

1. The previous step produced archive `result.zip`. Extract the archive to `~/ex1/_jplag_results`. This should give you (at least) the following:

   * Directory `~/ex1/_jplag_results/files` with a subdirectory `<user>/<file>` per user, where `<file>` is the checked source file
   * Files `~/ex1/_jplag_results/<user>-<user>.json` with pairwise analysis results
   * File `~/ex1/_jplag_results/overview.json`

### Step 3: Postprocessing

**Note:** All scripts support `-h`/`--help` to show supported command-line options. However, not all scripts are yet fully configurable! It is therefore best to run the scripts with default file and directory names, e.g. for where to store intermediate results.

1. Run `cxp-init`. It create a directory (`~/ex1/_static`) with files necessary for the next steps.

1. Run `cxp-diff`. It creates HTML files with compared submissions placed next to each other, and highlighted code blocks that JPlag considers equivalent.

1. Run `cxp-cluster`. It computes clusters of plagiarism, creates corresponding DOT and SVG files (by running GraphViz, which *must be in the path*), and creates a summarising CSV file.

   * Option ` --threshold-percentage <percentage>`: if provided, sets similarity percentage for when to include submissions in a cluster.

1. Run `cxp-aggr`. It combines the cluster data with exports from eDoz and Code Expert, and generates a cluster report as an HTML file.

   * Code Expert export: *My Courses* → *Students (Student Overview)* → *Export to CSV*. Pass file to `cxp-aggr` via the mandatory `--code-expert-export` option.

   * eDoz exports: *Communication/List* → *Course units* → *<Your course>* → *Export data (Zip/Text)*, then extract CSV (`.txt`) file from downloaded ZIP file. Pass files to `cxp-aggr` via the mandatory `--edoz-exports` option.

   * Option `--add-details <path/to/submissions>`: if provided, further information will be read from each submission's `details.json`.

1. **Optional:** run `cxp-mails`. It creates an e-mail per identified cluster, from the template file `_static/cluster_mail.txt` (e.g. `~/ex1/_static/cluster_mail.txt`). The e-mails are written to an Mbox file, as used by Thunderbird. If you pass your local Thunderbird outbox' unsent messages file to `cxp-mails` via the mandatory `--mbox-file` option (e.g. `~/thunderbird/xxxxxxxx.profile/Mail/Local Folders/Unsent Messages`), then you should afterwards be able to directly send the mails from inside Thunderbird via *File* → *Send Unsent Messages*. You can also review and edit the messages beforehand, they should be visible in the *Outbox* folder of your *Local Folders* account.
