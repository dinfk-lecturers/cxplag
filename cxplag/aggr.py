import os
import logging
import jinja2
import argparse
import datetime
import json
import pathlib
import pandas as pd
import textwrap
from dataclasses import dataclass
from .utils import logging as logutils

DEFAULT_COMPARISONS_DIRNAME="_diffs"
DEFAULT_CLUSTER_FILES_DIR="_clusters"
DEFAULT_CLUSTERS_MATCHES_CSV_FILE="clusters-matches.csv"
DEFAULT_CLUSTERS_STUDENTS_CSV_FILE="clusters-students.csv"
DEFAULT_CLUSTER_STUDENTS_CSV_FILE_PATTERN="cluster-students-{}.csv"
DEFAULT_JINJA_CLUSTER_TEMPLATE_FILE="./_static/clusters.html.jinja"

@dataclass
class RelativeDataPoint:
  name: str # Data point's name
  part: float # Relevant elements (i.e. numerator)
  base: float # All elements (i.e. denominator)

  @property
  def percentage(self, round_to_digits=1):
    return round(self.part * 100 / self.base, 1)

## TODO: Refactor function into separate ones
def aggregate(
    edoz_exports, # List of argparse.FileType objects
    cx_course_students_csv_file, # Single argparse.FileType object
    path_to_details_dir=None,
    cluster_files_dir=DEFAULT_CLUSTER_FILES_DIR,
    clusters_matches_csv_file=DEFAULT_CLUSTERS_MATCHES_CSV_FILE,
    clusters_students_csv_file=DEFAULT_CLUSTERS_STUDENTS_CSV_FILE,
    cluster_students_csv_file_pattern=DEFAULT_CLUSTER_STUDENTS_CSV_FILE_PATTERN,
    jinja_cluster_template_file=DEFAULT_JINJA_CLUSTER_TEMPLATE_FILE):

  logutils.configure_level_and_format()

  if not os.path.isfile(clusters_matches_csv_file):
    raise RuntimeError("CSV file {} with matches per clusters doesn't exist. Should have been created by cxp-cluster.".format(clusters_matches_csv_file))

  clusters_csv: pd.DataFrame = pd.read_csv(clusters_matches_csv_file)
  
  # Read CX course data, reduce to relevant columns, truncate TotalScore (which are floats), set index column
  relevant_course_columns = ["Legi", "Lastname", "Firstname", "Email", "Groupname", "Gender", "TotalScore"]
  course_csv: pd.DataFrame = pd.read_csv(cx_course_students_csv_file)
  course_csv = course_csv[relevant_course_columns]
  course_csv["TotalScore"] = course_csv["TotalScore"].round(0)
  course_csv.set_index("Legi", inplace=True)

  ## NOTE: Legi 00-000-000 used to indicate staff, but staff now seems to have no/NaN legi numbers
  # course_csv.drop("00-000-000", errors="ignore", inplace=True)

  duplicated_indices: pd.Index = course_csv[course_csv.index.duplicated()].index
  if (duplicated_indices.size > 0):    
    logging.warn(f"Found {len(duplicated_indices.values)} duplicated Legi numbers. These rows will be ignored:\n\n{textwrap.indent(course_csv[course_csv.index.duplicated()].to_string(), '    ')}")
    course_csv.drop(duplicated_indices, errors="ignore", inplace=True)


  ## TODO: Could integrate eDoz data "Leistungskontrollen" to get information whether
  ##       or not a student is a repeater  

  individual_edoz_csv_frames = []
  for csvfile in edoz_exports:
    relevant_edoz_columns = ["Nummer", "Departement"]
    edoz_csv: pd.DataFrame = pd.read_csv(csvfile, sep="\t")
    edoz_csv = edoz_csv[relevant_edoz_columns]
    edoz_csv.rename(columns={"Nummer": "Legi"}, inplace=True)
    edoz_csv.set_index("Legi", inplace=True)    
    # print(edoz1_csv)
    # print("edoz1_csv.index.is_unique = {}".format(edoz1_csv.index.is_unique))    

    individual_edoz_csv_frames.append(edoz_csv)

  # Vertically concat eDoz data. Since students may be enrolled into multiple
  # courses, duplicated rows are afterwards dropped.
  edoz_csv: pd.DataFrame = pd.concat(individual_edoz_csv_frames)
  # print("========== edoz_csv [initial]")
  # print(edoz_csv.shape)
  # print(edoz_csv)
  # edoz_csv.drop_duplicates(inplace=True) # Not applicable here since indices are ignored
  edoz_csv = edoz_csv.loc[~edoz_csv.index.duplicated(keep='first')] # Get rows not in the set of duplicated indices
  # print("========== edoz_csv [unique]")
  # print(edoz_csv.shape)
  # print(edoz_csv)


  ## TODO: Add "Departement" column to course_csv, by joining with edoz_csv


  ### Aggregate course overview statistics
  edoz_departements: pd.DataFrame = edoz_csv["Departement"].value_counts()
  course_genders: pd.DataFrame = course_csv["Gender"].value_counts()

  assert edoz_csv.index.is_unique, "Expected unique indices (= legis) in edoz_csv"
  # # Show rows with non-unique indices (https://stackoverflow.com/questions/20199129) 
  # print(edoz_csv[edoz_csv.index.duplicated(keep=False)])
  

  jinja2_file_loader = jinja2.FileSystemLoader(".")
  jinja2_env = jinja2.Environment(loader=jinja2_file_loader)

  try:
    template = jinja2_env.get_template(jinja_cluster_template_file)
  except jinja2.exceptions.TemplateNotFound as exception:
    raise RuntimeError("Couldn't load Jinja2 template {}. Should have been created by cxp-init.".format(jinja_cluster_template_file))   

  # output = template.render(colors=colors)
  # print(output)

  jinja2_rows = []

  cluster_groups: pd.DataFrameGroupBy = clusters_csv.groupby("cluster_id")

  for cluster_id, cluster in cluster_groups: # cluster: pd.DataFrame
    # print("-"*60)
    # Get all ids (= legis) participating in a cluster
    ids_values: numpy.ndarray = pd.concat([cluster["id1"], cluster["id2"]]).unique()
    
    # ids = pd.Series(ids_values, name="Legi", index=ids_values)
    # # Performs an inner join on the keys; here: legis
    # # https://pandas.pydata.org/pandas-docs/stable/getting_started/comparison/comparison_with_sql.html#compare-with-sql-join
    # join = pd.merge(ids, course_csv, left_index=True, right_index=True)

    # Select rows for list of indices ids_values.
    # If there is no row for a given index — e.g. when a master solution was send
    # to MOSS as an additional submission — a row with all NaNs is returned.
    # See also https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#deprecate-loc-reindex-listlike.
    cluster_course_rows: pd.DataFrame = course_csv.reindex(ids_values)
    # Preceding row selection is equivalent to the following, iff all indices exist:
    #   cluster_course_rows: pd.DataFrame = course_csv.loc[ids_values]

    # print("========== cluster ")
    # print(cluster.shape)
    # print(cluster)
    # print("========== ids_values ")
    # print(ids_values.shape)
    # print(ids_values)
    # print("========== course_csv")
    # print(course_csv)
    # print("========== cluster_course_rows")
    # print(cluster_course_rows.shape)
    # print(cluster_course_rows)
    # print("========== edoz_csv")
    # print(edoz_csv.shape)
    # print(edoz_csv)

    cluster_rows: pd.DataFrame = cluster_course_rows.join(edoz_csv)

    students_per_clusters_file = os.path.join(
        cluster_files_dir, 
        cluster_students_csv_file_pattern.format(cluster_id))

    if path_to_details_dir:
      # Add additional column
      cluster_rows.insert(3, "SubmissionDate", value="")

      # Read details.json per ID in current cluster
      for id in ids_values:
        details_json_filename = "{}/{}/details.json".format(path_to_details_dir, id)
        details_json_file = pathlib.Path(details_json_filename)

        if details_json_file.exists() and details_json_file.is_file():
          with open(details_json_filename, encoding="utf-8") as details_fh:
            details = json.load(details_fh)
            assert details["legi"] == id, "Mismatch between legi number {} from {} and current id {}".format(details["legi"], details_json_filename, id)

            ## Replacing "Z" with "+00:00" is due to https://discuss.python.org/t/parse-z-timezone-suffix-in-datetime
            cluster_rows.at[id, "SubmissionDate"] = \
              datetime.datetime \
                .fromisoformat(details["submissionDate"].replace("Z", "+00:00")) \
                .strftime("%Y-%m-%d %H:%M")
        else:
          logging.debug("File {} does not exist".format(details_json_filename))

    logging.info("Writing students from cluster {} to file {}".format(cluster_id, students_per_clusters_file))
    cluster_rows.to_csv(students_per_clusters_file)

    # print("========== cluster_rows")
    # print(cluster_rows.shape)
    # print(cluster_rows)
    # print(name)

    # print(cluster)
    # print(cluster["svg_file"].iat[0])

    jinja2_rows.append((cluster_id, cluster_rows.shape[0], cluster, cluster_rows))


  logging.info("Writing all clusters to file {}".format(clusters_students_csv_file))
  write_header = True
  write_mode = "w"
  for cluster_id, _, _, cluster_rows in jinja2_rows:
    cluster_rows["Cluster-ID"] = cluster_id ## Inserts column add end
    # cluster_rows.insert(0, "Cluster-ID", cluster_id) ## Inserts column after index (Legi)
    cluster_rows.to_csv(clusters_students_csv_file, mode=write_mode, header=write_header)
    write_header = False
    write_mode = "a"
  

  ## TODO: Support sorting clusters by max (or average) involved percentage


  plagiarist_count = 0
  for _, size, _, cluster_rows in jinja2_rows:
    plagiarist_count += size # cluster_rows.shape[0]


  department_counts = {}
  for _, _, _, cluster_rows in jinja2_rows:
    for index, value in cluster_rows["Departement"].value_counts().items():
      if index in department_counts:
        department_counts[index] += value
      else:
        department_counts[index] = value

  # print(department_counts)

  department_data = []
  for dep in department_counts:
    data = RelativeDataPoint(
      dep, 
      department_counts[dep], 
      edoz_departements[dep])
    
    department_data.append(data)
  
  # print(department_data)


  gender_counts = {}
  for _, _, _, cluster_rows in jinja2_rows:
    for index, value in cluster_rows["Gender"].value_counts().items():
      if index in gender_counts:
        gender_counts[index] += value
      else:
        gender_counts[index] = value

  # print(gender_counts)

  gender_data = []
  for dep in gender_counts:
    data = RelativeDataPoint(
      dep, 
      gender_counts[dep],
      course_genders[dep])
    
    gender_data.append(data)    
  
  # print(gender_data)

  datapoints = department_data + gender_data
  # print(datapoints)


  column_rename_map = {
    "Lastname": "Last", 
    "Firstname": "First",    
    "Groupname": "Group", 
    "Gender": "Gnd.", 
    "TotalScore": "Tot. score",
    "Departement": "Dept.",
    "Cluster-ID": "CID",
  }

  if path_to_details_dir:
    column_rename_map["SubmissionDate"] = "Sub. date"

  # Final preparations before creating the HTML report
  for cluster_id, _, _, cluster_rows in jinja2_rows:
    # Convert total scores from floats to integers, to reduce output width.
    # Total scores are effectively integers, anyway.
    # fillna(0) replaces each NA/NaN with zero; this is done to prevent errors, 
    # since NA/NaN cannot be converted to int.
    cluster_rows["TotalScore"] = \
      cluster_rows["TotalScore"].fillna(0).astype(int)
    
    if path_to_details_dir:
      cluster_rows.sort_values(by=["SubmissionDate"], inplace=True)

    # Rename columns to reduce output width.
    cluster_rows.rename(columns=column_rename_map, inplace=True)

  template.stream(
    title="Clusters",
    timestamp=datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),
    clusters=jinja2_rows,
    edoz_count=edoz_csv.shape[0],
    course_count=course_csv.shape[0],
    plagiarist_count=plagiarist_count,
    datapoints=datapoints
  ).dump("clusters.html")


def configure_cli_parser(parser):
  parser.add_argument(
    "-ee", "--edoz-exports",
    type=argparse.FileType("r", encoding="utf-8"),
    nargs="+",
    help="eDoz student list exports (CSV)",
    required=True)

  parser.add_argument(
    "-ce", "--code-expert-export",
    type=argparse.FileType("r", encoding="utf-8"),
    help="Code Expert student data export (CSV)",
    required=True)
  
  parser.add_argument(
    "-ad", "--add-details",
    type=str,
    help="Path to details.json files, e.g. './submissions'. Files are then expected in '<path>/<id>/details.json', e.g. './submissions/01-234-567/details.json'.",
    required=False)

  logutils.add_loglevel_argument(parser)


def main(
    cluster_files_dir=DEFAULT_CLUSTER_FILES_DIR,
    clusters_matches_csv_file=DEFAULT_CLUSTERS_MATCHES_CSV_FILE,
    clusters_students_csv_file=DEFAULT_CLUSTERS_STUDENTS_CSV_FILE,
    cluster_students_csv_file_pattern=DEFAULT_CLUSTER_STUDENTS_CSV_FILE_PATTERN,
    # cx_course_students_csv_file=DEFAULT_CX_COURSE_STUDENTS_CSV_FILE,
    jinja_cluster_template_file=DEFAULT_JINJA_CLUSTER_TEMPLATE_FILE):

  parser = argparse.ArgumentParser()
  configure_cli_parser(parser)
  args = parser.parse_args()

  logutils.configure_level_and_format(args.log_level)    

  aggregate(
    args.edoz_exports,
    args.code_expert_export,
    args.add_details,
    cluster_files_dir,
    clusters_matches_csv_file,
    clusters_students_csv_file,
    cluster_students_csv_file_pattern,
    jinja_cluster_template_file)


if __name__ == "__main__":
  main()
