#!/bin/bash

function quit_or_continue() {
  read -p "Press Q to abort, any other key to continue ..." -n 1 -s KEY
  echo
  if [[ ${KEY^^} == "Q" ]]; then exit; fi # https://stackoverflow.com/a/32210715
}


shopt -s globstar # Enable glob patterns such as **/details.json
first="true"

for i in **/details.json; do # Whitespace-safe and recursive
  echo "$i"
  legiNumber="$(jq -r .legi $i)" # https://stedolan.github.io/jq/
  
  command="mv ./$(dirname $i)/ ./$legiNumber/"

  if [[ $first == "true" ]]; then
    echo "Current directory: $(pwd)"
    echo "First directory will be renamed as follows (all others analogously):"
    echo $command
    quit_or_continue
    first="false"
  fi

  $command
done
