from setuptools import setup

setup(
  name='cxplag',
  version='0.0.3',
  description='Scripts for checking Code Expert submissions for code plagiarism',
  url='https://gitlab.ethz.ch/scmalte/mossutils.git',
  author='Malte Schwerhoff and Carlos Cotrini',
  author_email='malte.schwerhoff@inf.ethz.ch',
  license='unlicensed',
  packages=[
    'cxplag',
    'cxplag.utils'
  ],
  package_data={
    "cxplag": ["data/*"]
  },
  install_requires=[
    'pydot',
    'networkx',
    'pandas',
    'Jinja2',
    'Pygments'
  ],
  entry_points = {
    "console_scripts": [
      'cxp-init = cxplag.init:main',
      'cxp-diff = cxplag.diff:main',
      'cxp-cluster = cxplag.cluster:main',
      'cxp-aggr = cxplag.aggr:main',
      'cxp-mails = cxplag.mails:main'
    ]
  },
  zip_safe=False)
