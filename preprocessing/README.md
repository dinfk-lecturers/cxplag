# README

## Prerequisites

* Script `rename_to_legi.sh`:
  * https://stedolan.github.io/jq/
  * Download `jq` and add to path
  * TODO: Re-implement shell script in Python

## Tidying up files and directories

### CX course export with cherry-picked submissions

* Assumptions:

  * `./<submissions>/<user>` are the individual user directories, e.g. `submissions/scmalte`

  * Relevant files per submission are `<user>/submission_<timestamp1>/<timestamp2>_details.json` and `<user>/submission_<timestamp1>/project/main.cpp`, whereas all other files and directories can be deleted.
  
     Due to **regradings**, the directory name might change from `submission_<timestamp>` to `regrading_<timestamp>`; if so, adopt the commands below accordingly.

* **Optional** file merging:

  * If students had to edit multiple files, these need to be merged into a single file first

  * The following merges, per submission, the two files `project/main.cpp` and `project/node.cpp` into a new file `project/merged.cpp`:

     ```plain
     $ cd ./submissions

     $ find . -type d -iwholename '*Z/project' -exec sh -c 'cat {}/main.cpp {}/node.cpp > "{}/merged.cpp"' \;
     ```

  * In subsequent steps, use the newly created `merged.cpp`

* Delete irrelevant files, and directory `cx_description`:

  ```plain
  $ cd ./submissions

  $ find . -type f ! \( -iname *_details.json -or -iname main.cpp \) -delete -print
  
  $ find . -type d -iname cx_description -print -delete
  ```

* Move `<user>/submission_<timestamp1>/<timestamp2>_details.json` to `<user>/details.json`

  ```plain
  $ cd ./submissions

  $ find . -type f -iname *_details.json -print -execdir mv {} ../details.json \;
  ```

* Move `<user>/submission_<timestamp1>/project/main.cpp` to `<user>/main.cpp`, and then delete the (now empty) directory `<user>/submission_<timestamp1>`:

  ```plain
  $ cd ./submissions

  $ find . -type f -iname main.cpp -print -execdir mv {} ../../main.cpp \;

  $ find . -type d -iname submission_* -print -execdir rm -rf \{\} +
  ```

  **Note:** The last command may produce errors such as `.../project: No such file or directory`, which can be ignored.

* Now, each `<user>` directory should only have two files in it: `<user>/main.cpp` and `<user>/details.json`

## Renaming user directories

* Rename directories from user names to Legi numbers, e.g. rename `scmalte` to `01-234-567`. The file `<user>/details.json` provides the Legi number.

* Execute `rename_to_legi.sh` from e.g. `./submissions/`:

  ```plain
  $ cd ./submissions

  $ <path-to-cxplag>/preprocessing/rename_to_legi.sh
  ```
  
  The script prompts for confirmation before the first renaming is executed.
  